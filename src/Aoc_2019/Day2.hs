module Aoc_2019.Day2 where

-- import Helper

-- doStuff :: [Int] -> [Int] -> [Int]
-- doStuff [1, i1, i2, r] list = replaceNth r (list !! i1 + list !! i2) list
-- doStuff [2, i1, i2, r] list = replaceNth r (list !! i1 * list !! i2) list
-- doStuff _ list = list

-- -- 0 based step
-- proced1step :: Int -> [Int] -> [Int]
-- proced1step step list = doStuff cmd list
--   where
--     cmd = take 4 . drop (step * 4) $ list

-- lineToArray = map (\x -> read x :: Int) . words . replace "," ' ' . head

-- plsWork :: Int -> Int -> [Int] -> [Int]
-- plsWork max n list
--   | (list !! (n * 4)) == 99 = list
--   | n == max = list
--   | otherwise = plsWork max (n + 1) (proced1step n list)

-- part1NoStop list = plsWork ((length list) `div` 4) 0 list

-- part1 = part1NoStop <$> replaceNth 2 2 <$> replaceNth 1 12 <$> lineToArray <$> read2019 2

-- --  proced1step n $ part1NoStop list (n-1)