{-# OPTIONS_GHC -Wno-overlapping-patterns #-}

module Aoc_2021.Day2 where

import Helper

data Commande = Up Int | Down Int | Forward Int deriving (Show)

commande ("forward", value) = Forward value
commande ("down", value) = Down value
commande ("up", value) = Up value
commande (str, value) = error "Commande incorecte"

listToCmd = map commande . inputToPair

data SubMarine = SubMarine
  { depth :: Int,
    horizontal :: Int,
    aim :: Int
  }
  deriving (Show)

apply :: SubMarine -> Commande -> SubMarine
apply sub (Up cmd) = SubMarine {depth = depth sub, horizontal = horizontal sub, aim = aim sub - cmd}
apply sub (Down cmd) = SubMarine {depth = depth sub, horizontal = horizontal sub, aim = aim sub + cmd}
apply sub (Forward cmd) = SubMarine {depth = depth sub + aim sub * cmd, horizontal = horizontal sub + cmd, aim = aim sub}
apply sub cmd = error "miss match"

sumByWord :: String -> [String] -> Int
sumByWord tag = sum . map snd . filter ((== tag) . fst) . inputToPair

finalSubMarine :: Foldable t => t Commande -> SubMarine
finalSubMarine = foldl apply (SubMarine 0 0 0)

subToResult :: SubMarine -> Int
subToResult sub = depth sub * horizontal sub

part1 :: [String] -> Int
part1 list = (down - up) * forward
  where
    up = sumByWord "up" list
    down = sumByWord "down" list
    forward = sumByWord "forward" list

day2021_2 :: IO ()
day2021_2 = do
  putStrLn "Part 1 : "
  p1 <- part1 <$> readAOC 2
  print p1
  putStrLn "Part 2 : "
  p2 <- subToResult . finalSubMarine . listToCmd <$> readAOC 2
  print p2
