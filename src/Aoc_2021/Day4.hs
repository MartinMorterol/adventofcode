module Aoc_2021.Day4 where

import Data.Maybe (catMaybes)
import Data.Monoid
import Helper
import Test.QuickCheck.Text (number)

input = ["7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1", "", "22 13 17 11  0", " 8  2 23  4 24", "21  9 14 16  7", " 6 10  3 18  5", " 1 12 20 15 19", "", " 3 15  0  2 22", " 9 18 13 17  5", "19  8  7 25 23", "20 11 10 24  4", "14 21 16 12  6", "", "14 21 17 24  4", "10 16 15  9 19", "18  8 23 26 20", "22 11 13  6  5", " 2  0 12  3  7"]

-- formatedInput :: [String] -> ([Int], [[a]])

-- formatBoard :: [String] -> [[Maybe Int]]
formatBoard = map (map (Just . toInt) . words)

-- formatedInput :: [[Char]] -> ([Int], [[Maybe Int]])
formatedInput :: [[Char]] -> ([Int], [[[Maybe Int]]])
formatedInput xs = (numbers, boards)
  where
    splited = splitOn "" xs
    numbers = toInts . splitOn ',' . head $ head splited
    boards = map formatBoard $ tail splited

removeValue :: Eq a => a -> Maybe a -> Maybe a
removeValue y (Just x)
  | y == x = Nothing
  | otherwise = Just x
removeValue y Nothing = Nothing

checkNumerLine :: Eq a => a -> [Maybe a] -> [Maybe a]
checkNumerLine x = map (removeValue x)

checkBoard :: Eq a => a -> [[Maybe a]] -> [[Maybe a]]
checkBoard x = map (checkNumerLine x)

checkBingo :: Eq a => a -> [[[Maybe a]]] -> [[[Maybe a]]]
checkBingo x = map (checkBoard x)

toValue :: Num p => Maybe p -> p
toValue (Just x) = x
toValue Nothing = 0

isLineWin line = (sum . catMaybes $ line) == 0

isBoardWin :: (Foldable t, Eq a, Num a) => t [Maybe a] -> Bool
isBoardWin = any isLineWin

isBingoWin :: (Foldable t, Eq a, Num a) => t [[Maybe a]] -> Bool
isBingoWin = any isBoardWin

getWinner :: [[[Maybe Int]]] -> [[Maybe Int]]
getWinner = head . filter isBoardWin

sumWinner :: [[Maybe Int]] -> Int
sumWinner = sum . map (sum . catMaybes)

part1 :: [Int] -> [[[Maybe Int]]] -> Int
part1 numbers boards
  | winned = number * sumWinner (getWinner newBoards)
  | otherwise = part1 (tail numbers) newBoards
  where
    number = head numbers
    newBoards = checkBingo number boards
    winned = isBingoWin newBoards

day2021_4 :: IO ()
day2021_4 = do
  putStrLn "Part 1 : "
  (nubmers, boards) <- formatedInput <$> readAOC 4
  print $ part1 nubmers boards
