module Aoc_2021.Day1 where

import Helper

part1_impl :: [Int] -> Int
part1_impl list = length $ filter (> 0) $ zipWith (-) inital copy
  where
    inital = list
    copy = head list : list

part1 :: IO Int
part1 = part1_impl . toInts <$> readAOC 1

triple :: [Int] -> [Int]
triple xs = zipWith (+) (drop 2 xs) $ zipWith (+) xs (drop 1 xs)

part2 :: IO Int
part2 = part1_impl . triple . toInts <$> readAOC 1

day2021_1 :: IO ()
day2021_1 = do
  p1 <- part1
  print p1
  p2 <- part2
  print p2