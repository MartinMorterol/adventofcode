module Aoc_2021.Day3 where

import Data.Char (digitToInt)
import Data.List (foldl')
import Helper

toDec :: String -> Int
toDec = foldl' (\acc x -> acc * 2 + digitToInt x) 0

firstValue :: String -> Int
firstValue (f : xs) = toInt [f]
firstValue xs = error "fuck"

firstValues :: [String] -> [Int]
firstValues = map firstValue

getValue :: Int -> String -> Int
getValue indice xs = toDigit $ xs !! indice

getValues :: Int -> [String] -> [Int]
getValues indice = map (getValue indice)

sumColumn :: [Int] -> [[String]] -> [Int]
sumColumn = zipWith (\i xs -> sum $ getValues i xs)

upperBool half = map (`div` half)

lowerBool :: Int -> [Int] -> [Int]
lowerBool half = map ((1 -) . (`div` half))

create2Daoc :: Int -> a -> [a]
create2Daoc = replicate

gamaValue :: Int -> [Int] -> [[String]] -> Int
gamaValue = getvalue upperBool

epsiValue :: Int -> [Int] -> [[String]] -> Int
epsiValue = getvalue lowerBool

getvalue :: (Int -> [Int] -> [Int]) -> Int -> [Int] -> [[String]] -> Int
getvalue selector half indices aocs = toDec . concatMap show . selector half $ sumColumn indices aocs

part1FromInput :: [String] -> Int
part1FromInput xs = gama * epsi
  where
    nbValue = length xs
    size = length (xs !! 0)
    half = nbValue `div` 2
    aocs = replicate size xs
    indices = [0 .. size -1]
    gama = gamaValue half indices aocs
    epsi = epsiValue half indices aocs

day2021_3 :: IO ()
day2021_3 = do
  putStrLn "Part 1 : "
  part1 <- part1FromInput <$> readAOC 3
  print part1
