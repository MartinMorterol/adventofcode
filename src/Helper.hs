module Helper where

import Data.List

toDigit :: Char -> Int
toDigit x = read [x] :: Int

toInt :: String -> Int
toInt x = read x :: Int

toInts :: [String] -> [Int]
toInts = map toInt

unique :: String -> String
unique = map head . group . sort

splitOn :: (Eq a) => a -> [a] -> [[a]]
splitOn _ [] = []
splitOn x ys = f : splitOn x rest
  where
    (f, rest) = break (== x) (dropWhile (== x) ys)

readAOC_ :: String -> Int -> IO [String]
readAOC_ folder jour =
  lines <$> readFile (folder ++ "inputD" ++ show jour ++ ".txt")

readAOC :: Int -> IO [String]
readAOC = readAOC_ "./inputs/2021/"

readShortAOC :: Int -> IO [String]
readShortAOC = readAOC_ "./inputs/2021/mini_"

read2019 :: Int -> IO [String]
read2019 = readAOC_ "./inputs/2019/"

replace' :: Eq a => [a] -> a -> [a] -> [a]
replace' olds new = map (\c -> if c `elem` olds then new else c)

xor :: Eq a => a -> a -> Bool
xor = (/=)

replaceNth :: Int -> a -> [a] -> [a]
replaceNth _ _ [] = []
replaceNth n newVal (x : xs)
  | n == 0 = newVal : xs
  | otherwise = x : replaceNth (n - 1) newVal xs

toPair :: [String] -> (String, Int)
toPair [cmd, value] = (cmd, read value :: Int)
toPair str = error "il devrait y avoir exactement 2 mots"

inputToPair :: [String] -> [(String, Int)]
inputToPair = map (toPair . words)
