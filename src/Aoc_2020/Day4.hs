module Aoc_2020.Day4 where

import Data.List
import Data.List.Split

readD4 s = lines <$> readFile s

input :: IO [String]
input = lines <$> readFile "inputD4.txt"

fields :: [String]
fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"] -- "cid"]

lineIntoPassport :: [String] -> [String]
lineIntoPassport = map (intercalate " ") . splitOn [""]

checkPassport :: String -> Bool
checkPassport passport = and $ map ($ passport) $ map isInfixOf fields

countValid :: [String] -> Int
countValid = length . filter (== True) . map (checkPassport) . lineIntoPassport

part1 :: IO Int
part1 = countValid <$> input

stringInRang s min_ max_ = elem s (map show [min_ .. max_])

charInRang min_ max_ c = elem c (map head $ map show [min_ .. max_])

isNumberOfSize s n = length (filter (charInRang 0 9) s) == n

pidCheck s = (length s == 9) && isNumberOfSize s 9

hclCheck s =
  (head s == '#')
    && length (filter ((flip $ elem) (['0' .. '9'] ++ ['a' .. 'f'])) s)
    == 6

cmCheck s =
  (length s == 5) && (stringInRang (take 3 s) 150 193) && ((drop 3 s) == "cm")

inCheck s =
  (length s == 4) && (stringInRang (take 2 s) 59 76) && ((drop 2 s) == "in")

hgtCheck s = cmCheck s || inCheck s

checkField :: [String] -> Bool
checkField ("byr" : xs) = stringInRang (head xs) 1920 2002
checkField ("iyr" : xs) = stringInRang (head xs) 2010 2020
checkField ("eyr" : xs) = stringInRang (head xs) 2020 2030
checkField ("pid" : xs) = pidCheck (head xs)
checkField ("hcl" : xs) = hclCheck (head xs)
checkField ("hgt" : xs) = hgtCheck (head xs)
checkField ("ecl" : xs) =
  elem (head xs) ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
checkField xs = True

toFields :: String -> [[String]]
toFields = map (splitOn ":") . words

checkPassport2 :: String -> Bool
checkPassport2 p = (and . map checkField . toFields $ p) && checkPassport p

countValid2 :: [String] -> Int
countValid2 =
  length . filter (== True) . map (checkPassport2) . lineIntoPassport

part2 :: IO Int
part2 = countValid2 <$> input
