module Aoc_2020.Day2 where

-- import InputD2

-- replace' :: (Foldable t, Eq b) => t b -> b -> [b] -> [b]
-- replace' olds new list = map (\c -> if (elem c olds) then new else c) list

-- xor = (/=)

-- checkPwdPart1 input = checkPwdImpl $ words $ replace' ":-" ' ' input
--   where
--     checkPwdImpl (strMin : strMax : [letter] : [pwd]) =
--       elem
--         nbLetter
--         [minVal .. maxVal]
--       where
--         nbLetter = length $ filter (letter ==) pwd
--         minVal = read strMin :: Int
--         maxVal = read strMax :: Int

-- checkPwdPart2 input = checkPwdImpl $ words $ replace' ":-" ' ' input
--   where
--     checkPwdImpl (strIndice1 : strIndice2 : [letter] : [pwd]) =
--       (xor)
--         (pwd !! i1 == letter)
--         (pwd !! i2 == letter)
--       where
--         i1 = (read strIndice1 :: Int) - 1
--         i2 = (read strIndice2 :: Int) - 1

-- solution checker = length $ filter (== True) $ map checker inputs

-- part1 = solution checkPwdPart1

-- part2 = solution checkPwdPart2
