module Aoc_2020.Day8 where

import Data.List
import Helper

inputFormat :: [String] -> [(String, Int)]
inputFormat = map $ toPair . words . replace' "+" ' '

calculPart1 :: Int -> [Int] -> Int -> [(String, Int)] -> Int
calculPart1 indice indicesVus total commandes
  | indice `elem` indicesVus = total
  | readCmd == "acc" =
    calculPart1
      (indice + 1)
      (indice : indicesVus)
      (total + value)
      commandes
  | readCmd == "jmp" =
    calculPart1
      (indice + value)
      (indice : indicesVus)
      (total)
      commandes
  | readCmd == "nop" =
    calculPart1
      (indice + 1)
      (indice : indicesVus)
      (total)
      commandes
  where
    readCmd = fst (commandes !! indice)
    value = snd (commandes !! indice)

part1 = calculPart1 0 [] 0 <$> inputFormat <$> readAOC 8

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

createSolutions_ :: [(String, Int)] -> Int -> [[(String, Int)]]
createSolutions_ candidates i
  | i >= length candidates =
    []
  | cmd == "jmp" =
    (replaceNth i (updatedCmd "nop") candidates) :
    (createSolutions_ candidates (i + 1))
  | cmd == "nop" =
    (replaceNth i (updatedCmd "jmp") candidates) :
    (createSolutions_ candidates (i + 1))
  | otherwise =
    createSolutions_ candidates (i + 1)
  where
    cmd = fst $ candidates !! i
    val = snd $ candidates !! i
    updatedCmd newCmd = (newCmd, val)

createSolutions :: [(String, Int)] -> [[(String, Int)]]
createSolutions = flip createSolutions_ 0

calculPart2 :: Int -> [Int] -> Int -> [(String, Int)] -> Maybe Int
calculPart2 indice indicesVus total commandes
  | indice `elem` indicesVus = Nothing
  | indice > (length commandes) = Nothing
  | indice == (length commandes) = Just total
  | readCmd == "acc" =
    calculPart2
      (indice + 1)
      (indice : indicesVus)
      (total + value)
      commandes
  | readCmd == "jmp" =
    calculPart2
      (indice + value)
      (indice : indicesVus)
      (total)
      commandes
  | readCmd == "nop" =
    calculPart2
      (indice + 1)
      (indice : indicesVus)
      (total)
      commandes
  where
    readCmd = fst (commandes !! indice)
    value = snd (commandes !! indice)

ipt :: [(String, Int)]
ipt =
  [ ("nop", 0),
    ("acc", 1),
    ("jmp", 4),
    ("acc", 3),
    ("jmp", -3),
    ("acc", -99),
    ("acc", 1),
    ("jmp", -4),
    ("acc", 6)
  ]

part2Unitaire :: [(String, Int)] -> Maybe Int
part2Unitaire sol = calculPart2 0 [] 0 sol

part2Total :: [String] -> Maybe Int
part2Total =
  head
    . filter (/= Nothing)
    . map part2Unitaire
    . createSolutions
    . inputFormat

part2 = part2Total <$> readAOC 8
