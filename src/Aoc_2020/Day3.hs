module Aoc_2020.Day3 where

-- import InputD3

-- inputs' :: [String]
-- inputs' =
--   [ "..##.......",
--     "#...#...#..",
--     ".#....#..#.",
--     "..#.#...#.#",
--     ".#...##..#.",
--     "..#.##.....",
--     ".#.#.#....#",
--     ".#........#",
--     "#.##...#...",
--     "#...##....#",
--     ".#..#...#.#"
--   ]

-- wide :: Int
-- wide = length . head $ inputs

-- checkTree :: (Int, String) -> Bool
-- checkTree (a, xs) = (xs !! a == '#')

-- countTree :: [(Int, String)] -> Int
-- countTree = length . filter checkTree

-- addPosition :: [String] -> [(Int, String)]
-- addPosition = zip (map (`mod` wide) [0, 3 ..])

-- part1 :: [String] -> Int
-- part1 = countTree . addPosition

-- smartLadyDream1 =
--   length . filter (\(a, xs) -> (xs !! a == '#'))
--     . zip
--       (map (`mod` wide) [0, 3 ..])

-- addPosition' :: Int -> [String] -> [(Int, String)]
-- addPosition' step = zip (map (`mod` wide) [0, step ..])

-- takeMod :: Int -> [a] -> [a]
-- takeMod n [] = []
-- takeMod n xs = head xs : (takeMod n $ drop n xs)

-- forOneSlope :: [String] -> (Int, Int) -> Int
-- forOneSlope xs (stepH, stepV) =
--   countTree $ addPosition' stepH $ takeMod stepV xs

-- part2 :: Int
-- part2 = product $ map (forOneSlope inputs) inputsSlopes
