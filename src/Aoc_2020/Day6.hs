module Aoc_2020.Day6 where

import Data.List
import Helper

calculPart1 :: [String] -> Int
calculPart1 = sum . map (length . unique . concat) . splitOn ""

part1 = calculPart1 <$> readAOC 6

calculPart2 :: [String] -> Int
calculPart2 = sum . map (length . foldl1 intersect) . splitOn ""

part2 :: IO Int
part2 = calculPart2 <$> readAOC 6
