module Aoc_2020.Day5 where

import Data.List

readAOC :: FilePath -> IO [String]
readAOC s = lines <$> readFile s

selectHalf :: String -> [Int] -> [Int]
selectHalf "up" [min_, max_] = [(div (sum [min_, max_]) 2) + 1, max_]
selectHalf "down" [min_, max_] = [min_, (div (sum [min_, max_]) 2)]
selectHalf _ _ = undefined

decodeChar :: Char -> String
decodeChar 'B' = "up"
decodeChar 'F' = "down"
decodeChar 'R' = "up"
decodeChar 'L' = "down"
decodeChar _ = undefined

decodeString :: String -> [String]
decodeString = map decodeChar

calculImpl :: [String] -> ([Int] -> [Int])
calculImpl xs = foldr1 (.) $ map selectHalf (reverse (xs))

calculRow :: String -> [Int]
calculRow tk = (calculImpl $ decodeString (fst (splitAt 7 tk))) [0, 127]

calculSeat :: String -> [Int]
calculSeat tk = (calculImpl $ decodeString (snd (splitAt 7 tk))) [0, 7]

getId :: String -> Int
getId tk = (head $ calculRow tk) * 8 + (head $ calculSeat tk)

maxId :: [String] -> Int
maxId = foldr1 max . map getId

part1 :: IO Int
part1 = maxId <$> readAOC "inputD5.txt"

mySeat :: [String] -> Int
mySeat xs = head $ [head $ sortedId .. maxId xs] \\ sortedId
  where
    sortedId = sort $ map getId xs

part2 :: IO Int
part2 = mySeat <$> readAOC "inputD5.txt"
